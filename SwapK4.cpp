/*
    C program for
    Computing the Swap ew-GENEO for K_4
*/
#include <stdio.h>

void show(int record[], int last)
{
	for (int i = 0; i < last; i++) {     
        printf("%d", record[i]);     
    }
}

void solution(int record[], int start, int last)
{
	if (start == last)
	{
		printf("\n");
		show(record, last);

		int recordTemp[last];
		int recordSum[last] = {0, 0, 0, 0, 0, 0};
		int temp;

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[1];
    	recordTemp[1] = recordTemp[2];
    	recordTemp[2] = temp;
    	temp = recordTemp[3];
    	recordTemp[3] = recordTemp[4];
    	recordTemp[4] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[0];
    	recordTemp[0] = recordTemp[1];
    	recordTemp[1] = temp;
    	temp = recordTemp[3];
    	recordTemp[3] = recordTemp[5];
    	recordTemp[5] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[0];
    	recordTemp[0] = recordTemp[4];
    	recordTemp[4] = temp;
    	temp = recordTemp[2];
    	recordTemp[2] = recordTemp[5];
    	recordTemp[5] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[0];
    	recordTemp[0] = recordTemp[2];
    	recordTemp[2] = temp;
    	temp = recordTemp[4];
    	recordTemp[4] = recordTemp[5];
    	recordTemp[5] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[0];
    	recordTemp[0] = recordTemp[3];
    	recordTemp[3] = temp;
    	temp = recordTemp[1];
    	recordTemp[1] = recordTemp[5];
    	recordTemp[5] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		for (int i = 0; i < last; i++) {     
        	recordTemp[i] = record[i];     
    	}
    	temp = recordTemp[1];
    	recordTemp[1] = recordTemp[4];
    	recordTemp[4] = temp;
    	temp = recordTemp[2];
    	recordTemp[2] = recordTemp[3];
    	recordTemp[3] = temp;    	
		printf("   ");				
		show(recordTemp, last);
		for (int i = 0; i < last; i++) {     
        	recordSum[i] = recordSum[i] + recordTemp[i];     
    	}		

		printf("   ");				
		show(recordSum, last);

		return;
	}

	record[start] = 0;
	solution(record, start + 1, last);
	// change to 1
	record[start] = 1;
	solution(record, start + 1, last);
}
void binaryString(int n)
{
	// N indicate digit in binary
	if (n <= 0)
	{
		return;
	}
	int record[n];
	printf(" We are considering the complete graph with %d vertices and %d edges: \n", 4, n);
	solution(record, 0, n);
}
int main(int argc, char const * argv[])
{
	// Test
	binaryString(6);
	return 0;
}




