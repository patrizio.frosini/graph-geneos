/*
    Generalized Permutant - D6D3.
	The program to compute the GENEO D6D3_F_2 for all subgraphs of C_6.
    The permutant considered: D6D3_2 = {153, 426}
*/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const * argv[])
{
	//The variables used for specifying all subgraphs of C_6 as bit strings of 0's and 1's.
	int i1, i2, i3, i4, i5, i6;
	
	//General purpose counters.
	int i, j, k;
	
	//Subgraphs of C_6 specified by assigning the weight 0 or 1 on each edge.
	int weights6[64][6];
	//For each subgraph, we will load its specification in this array for each permutation.
	int tempWeight3[3];
	//The final GENEO as a real valued function on E(C_6) for each subgraph.
	int weights3[64][3];
	
	//Compute the subgraphs of C_6 as all bit strings of 0's or 1's.
	i = 0;
	for(i1=0; i1<2; i1++)
	{
		for(i2=0; i2<2; i2++)
		{
			for(i3=0; i3<2; i3++)
			{
				for(i4=0; i4<2; i4++)
				{
					for(i5=0; i5<2; i5++)
					{
						for(i6=0; i6<2; i6++)
						{
							weights6[i][0] = i1;
							weights6[i][1] = i2;
							weights6[i][2] = i3;
							weights6[i][3] = i4;
							weights6[i][4] = i5;
							weights6[i][5] = i6;
							
							i++;
						}
					}
				}
			}
		}

	}
	
	//The elements of the permutant D6D3_2 as functions from E(C_3) to E(C_6).
	int functions[2][3] = {
							{1, 5, 3},
							{4, 2, 6}
	};

    // creating file pointer to work with files
    FILE *fptr;
	
	// opening file in writing mode
	fptr = fopen("D6D3_F_2.txt", "w");

    // exiting program 
    if (fptr == NULL) {
       	printf("Error!");
       	exit(1);
    }
	
	fprintf(fptr, " The generalized permutant D6D3_2 and its corresponding GENEO D6D3_F_2: \n\n");
		
	//Consider all subgraphs; use i for each subgraph, j for each f in D6D3_4, and k for each edge.
	for(i=0; i<64; i++)
	{
		//Print the subgraph as a bit string.
		for(k=0; k<6; k++)
		{
			fprintf(fptr, "%d", weights6[i][k]);
		}
		fprintf(fptr, ":    ");

		//Initialize the final GENEO, i.e., the function \psi on E(C_3), to 0.
		for(k=0; k<3; k++)
		{
			weights3[i][k] = 0;
		}

		//Compute and print gfT(g)^-1 for each f in D6D3_2, and update the final GENEO accordingly for each f.
		for(j=0; j<2; j++)
		{
			//Compute gfT(g)^-1 for each edge of C_3, and accumulate in the corresponding entry of the final GENEO after printing in the file.
			for(k=0; k<3; k++)
			{
				tempWeight3[k] = weights6[i][functions[j][k] - 1];
				fprintf(fptr, "%d", tempWeight3[k]);
				weights3[i][k] = weights3[i][k] + tempWeight3[k];
			}
			if(j != 1)
			{
				fprintf(fptr, " + ");
			}
		}//j
		
		//Print the GENEO in the file.
		fprintf(fptr, "   =  ");
		for(k=0; k<3; k++)
		{
			fprintf(fptr, " %d,", weights3[i][k]);
		}		
		fprintf(fptr, "\n");
		
		//Make blocks of 4 rows each.
		if(i%4 == 3)
		{
			fprintf(fptr, "\n");
		}
	}//i
	
	fclose(fptr);
	return 0;
}

