/*
    C program for
    Computing the swap ew-GENEO for K_5
*/
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const * argv[])
{
	int graphs = 34, vertices = 5, edges = 10;
	int i, j, k;
	
	int subgraphs[graphs][edges] = {
									{0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
									{1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
									{1, 0, 0, 0, 0, 0, 0, 0, 0, 0},
									{0, 1, 1, 1, 1, 1, 1, 1, 1, 1},
									{1, 1, 0, 0, 0, 0, 0, 0, 0, 0},
									{0, 0, 1, 1, 1, 1, 1, 1, 1, 1},
									{1, 0, 1, 0, 0, 0, 0, 0, 0, 0},
									{0, 1, 0, 1, 1, 1, 1, 1, 1, 1},
									{1, 1, 0, 0, 0, 0, 1, 0, 0, 0},
									{0, 0, 1, 1, 1, 1, 0, 1, 1, 1},
									{1, 1, 0, 1, 0, 0, 0, 0, 0, 0},
									{0, 0, 1, 0, 1, 1, 1, 1, 1, 1},
									{1, 1, 1, 0, 0, 0, 0, 0, 0, 0},
									{0, 0, 0, 1, 1, 1, 1, 1, 1, 1},
									{1, 1, 0, 0, 0, 1, 0, 0, 0, 0},
									{0, 0, 1, 1, 1, 0, 1, 1, 1, 1},
									{1, 0, 0, 0, 1, 1, 0, 0, 0, 1},
									{0, 1, 1, 1, 0, 0, 1, 1, 1, 0},
									{1, 1, 1, 0, 0, 0, 0, 0, 0, 1},
									{0, 0, 0, 1, 1, 1, 1, 1, 1, 0},
									{1, 1, 0, 1, 0, 0, 1, 0, 0, 0},
									{0, 0, 1, 0, 1, 1, 0, 1, 1, 1},
									{1, 1, 1, 0, 0, 1, 0, 0, 0, 0},
									{0, 0, 0, 1, 1, 0, 1, 1, 1, 1},
									{1, 1, 1, 1, 0, 0, 0, 0, 0, 0},
									{0, 0, 0, 0, 1, 1, 1, 1, 1, 1},
									{1, 1, 0, 1, 0, 1, 0, 0, 0, 0},
									{0, 0, 1, 0, 1, 0, 1, 1, 1, 1},
									{1, 1, 1, 1, 0, 0, 0, 0, 0, 1},
									{0, 0, 0, 0, 1, 1, 1, 1, 1, 0},
									{1, 1, 0, 0, 1, 1, 0, 0, 0, 1},
									{0, 0, 1, 1, 0, 0, 1, 1, 1, 0},
									{1, 1, 1, 0, 1, 1, 0, 0, 0, 0},
									{1, 1, 1, 1, 1, 0, 0, 0, 0, 0}
	};
	
	int permutations[edges][edges] = {
										{1, 6, 3, 4, 7, 2, 5, 10, 9, 8},
										{2, 1, 10, 4, 9, 6, 7, 8, 5, 3},
										{8, 2, 6, 5, 4, 3, 7, 1, 9, 10},
										{7, 2, 3, 10, 5, 9, 1, 8, 6, 4},
										{6, 2, 8, 4, 5, 1, 9, 3, 7, 10},
										{10, 3, 2, 7, 5, 6, 4, 8, 9, 1},
										{5, 9, 3, 8, 1, 6, 7, 4, 2, 10},
										{1, 8, 3, 9, 5, 10, 7, 2, 4, 6},
										{1, 7, 4, 3, 6, 5, 2, 8, 9, 10},
										{1, 2, 9, 4, 10, 6, 8, 7, 3, 5}
	};
	
	int permutedSubgraph[edges];
	int codes[graphs][edges];

    // creating file pointer to work with files
    FILE *fptr;
	
	// opening file in writing mode
	fptr = fopen("subgraphs_K5.txt", "w");

    // exiting program 
    if (fptr == NULL) {
       	printf("Error!");
       	exit(1);
    }
	
	fprintf(fptr, " We are considering the complete graph with %d vertices and %d edges: \n\n", vertices, edges);
		
	for(i=0; i<graphs; i++)
	{
		for(j=0; j<edges; j++)
		{
			fprintf(fptr, "%d", subgraphs[i][j]);
			codes[i][j] = 0;
		}
		fprintf(fptr, "  ");

		for(j=0; j<edges; j++)
		{
			for(k=0; k<edges; k++)
			{
				permutedSubgraph[k] = subgraphs[i][permutations[j][k] - 1];
				fprintf(fptr, "%d", permutedSubgraph[k]);
				codes[i][k] = codes[i][k] + permutedSubgraph[k];
			}
			fprintf(fptr, "  ");
		}
		
		for(j=0; j<edges; j++)
		{
			fprintf(fptr, "%d,", codes[i][j]);
		}		
		fprintf(fptr, "\n");
		if(i%2 == 1 || i == 32)
		{
			fprintf(fptr, "\n");
		}
	}
	
	fclose(fptr);
	return 0;
}

